#!/usr/bin/env python3

import argparse
import json
import logging
import os
import pathlib
import requests
import sys
import xml.etree.ElementTree as ET
import zlib

def strip_epoch(version):
    """Strips epoch string from a version string"""
    if ":" in version:
        version = version.split(":")[1]
    return version


def get_package_names_from_json(crjson, distro):
    """Extracts set of package names from a content-resolver json output"""
    packages = set()
    for pkg in crjson["pkg_query"]:
        pkg_name = pkg["name"]
        # Hardcoded corner cases for handling RHEL9 names
        if distro.startswith("rhel"):
            if pkg_name == "centos-stream-release":
                pkg_name = "redhat-release"
            elif pkg_name == "centos-logos":
                pkg_name = "redhat-logos"
        packages.add(pkg_name)
    return packages


def get_nvrs(repos):
    """ Resolve list of nvrs from a set of repos """
    nvrs = {}
    for repo in repos:
        # Avoid pulling lots of deps by just parsing the xml directly
        # Find primary.xml via repomd.xml
        logging.debug("Fetching repomd: %s/repodata/repomd.xml", repo)
        resp = requests.get(f"{repo}/repodata/repomd.xml")
        repomd = ET.fromstring(resp.content)
        ns = "{http://linux.duke.edu/metadata/repo}"
        primary_data = repomd.find(f"{ns}data[@type='primary']/{ns}location")
        primary_xml_path = primary_data.attrib["href"]
        logging.debug("Primary xml: %s", primary_xml_path)

        # Fetch primary.xml and parse package tags
        logging.debug("Handling primary: %s/%s", repo, primary_xml_path)
        resp = requests.get(f"{repo}/{primary_xml_path}")
        if primary_xml_path.endswith(".gz"):
            primary_xml = zlib.decompress(resp.content, 16+zlib.MAX_WBITS)
        else:
            primary_xml = resp.content
        ns = "{http://linux.duke.edu/metadata/common}"
        primary = ET.fromstring(primary_xml)

        # Iterate through the package elements
        # If there are multiple versions of a package, they're luckily sorted
        # from oldest to newest so the final nevra is the newest
        for package in primary:
            if package.tag != f"{ns}package":
                continue
            name = package.find(f"./{ns}name")
            full_version = package.find(f"./{ns}version")
            ver = full_version.attrib["ver"]
            rel = full_version.attrib["rel"]
            arch = package.find(f"./{ns}arch")

            nvrs[name.text] = f"{ver}-{rel}"

        logging.debug("Resolved: %d packages", len(primary))

    return nvrs


def main():
    parser = argparse.ArgumentParser(
        description="Convert tiny.distro.builders jsons to lockfile json"
    )
    parser.add_argument("--distro", type=str, default="cs9", choices=["cs9", "rhel9"])
    parser.add_argument("--env", type=str, nargs='+', default="", help="URLs or paths to content-resolver environment jsons")
    parser.add_argument("--repos", type=str, nargs='+', default="", help="Yum repos to resolve nvr's from")
    parser.add_argument("--debug", action='store_true', help="Print debug messages to stderr")
    parser.add_argument("--outfile", nargs='?', type=str, help="Output file in json format")
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    # Set some default env json urls if none are manually set
    crjson_envs = args.env
    if not crjson_envs:
        crjson_envs = [
            "https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--aarch64.json",
            "https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--x86_64.json",
            "https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--aarch64.json",
            "https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--x86_64.json",
        ]

    repos = args.repos
    if not repos:
        if args.distro == "rhel9":
            repos = [
                "http://download.eng.brq.redhat.com/rhel-9/nightly/RHEL-9/latest-RHEL-9/compose/BaseOS/aarch64/os",
                "http://download.eng.brq.redhat.com/rhel-9/nightly/RHEL-9/latest-RHEL-9/compose/BaseOS/x86_64/os",
                "http://download.eng.brq.redhat.com/rhel-9/nightly/RHEL-9/latest-RHEL-9/compose/AppStream/aarch64/os",
                "http://download.eng.brq.redhat.com/rhel-9/nightly/RHEL-9/latest-RHEL-9/compose/AppStream/x86_64/os",
                "http://download.eng.brq.redhat.com/rhel-9/nightly/RHEL-9/latest-RHEL-9/compose/CRB/aarch64/os",
                "http://download.eng.brq.redhat.com/rhel-9/nightly/RHEL-9/latest-RHEL-9/compose/CRB/x86_64/os",
            ]
        else:
            repos = [
                "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/BaseOS/aarch64/os/",
                "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/BaseOS/x86_64/os/",
                "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/AppStream/aarch64/os/",
                "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/AppStream/x86_64/os/",
                "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/CRB/aarch64/os/",
                "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/CRB/x86_64/os/",
            ]

    # Initiate empty sets
    packages_aarch64 = set()
    packages_x86 = set()

    # Get nvrs from repos
    logging.debug("Grabbing nvrs from repos: %s", repos)
    nvrs = get_nvrs(repos)
    logging.debug("%d nvrs resolved.", len(nvrs))

    # Cornercase: force librhsm into packages sets if handling rhel9 distro
    if args.distro == "rhel9":
        packages_aarch64.add(f"librhsm-{nvrs['librhsm']}")
        packages_x86.add(f"librhsm-{nvrs['librhsm']}")

    # Parse env json files
    # Download if url otherwise open from local filesystem
    for crjson_env in crjson_envs:
        if crjson_env.startswith("http"):
            logging.debug("Downloading: %s", crjson_env)
            resp = requests.get(crjson_env)
            logging.debug("Downloaded: %s", resp.url)
            env_json = resp.json()
        else:
            logging.debug("Loading: %s", crjson_env)
            with open(crjson_env) as json_file:
                env_json = json.load(json_file)
            logging.debug("Loaded.")

        logging.debug("Parsing env json.")
        package_names = get_package_names_from_json(env_json, args.distro)
        logging.debug("%d packages in environment.", len(package_names))

        # Resolve nvr from repos
        packages = set()
        for package_name in package_names:
            if package_name in nvrs:
                packages.add(f"{package_name}-{nvrs[package_name]}")
            else:
                # centos-gpg-keys and centos-stream-repos currently fail as they're
                # part of the product definition but not part of the centos compose
                logging.warning("No nevra available for package %s", package_name)

        # Update package sets
        env_file = os.path.basename(crjson_env)
        if "aarch64" in env_file:
            packages_aarch64.update(packages)
        elif "x86_64" in env_file:
            packages_x86.update(packages)
        else:
            raise Exception(f"Invalid package architecture for environment file: {env_file}")

    # Determine the common and architecture unique packages using set maths
    common = packages_x86.intersection(packages_aarch64)
    x86 = packages_x86.difference(common)
    aarch64 = packages_aarch64.difference(common)

    # Generate the package manifests json file for automotive-sig
    # See: https://gitlab.com/redhat/automotive/automotive-sig/-/blob/main/package_list/cs9-image-manifest.lock.json
    output = {
        args.distro: {
            "common": sorted(list(common), key=str.lower),
            "arch": {
                "aarch64": sorted(list(aarch64), key=str.lower),
                "x86_64": sorted(list(x86), key=str.lower),
            },
        }
    }

    if args.outfile:
        with open(args.outfile, "w", encoding="utf-8") as f:
            json.dump(output, f, indent=4)
    else:
        print(json.dumps(output, indent=4))


if __name__ == "__main__":
    main()
