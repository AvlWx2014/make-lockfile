#!/bin/bash
podman build -t make-lockfile:latest .

# Generate cs9-image-manifest.lock.json
podman run --privileged -v $(pwd):/mnt -t make-lockfile:latest \
  --debug \
  --distro cs9 \
  --env \
  https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--aarch64.json \
  https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--x86_64.json \
  https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--aarch64.json \
  https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--x86_64.json \
  --outfile /mnt/cs9-image-manifest.lock.json

# Generate rhel9-image-manifest.lock.json
podman run --privileged -v $(pwd):/mnt -t make-lockfile:latest \
  --debug \
  --distro rhel9 \
  --env \
  https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--aarch64.json \
  https://tiny.distro.builders/workload--automotive-workload-in--automotive-environment--automotive-repositories-c9s--x86_64.json \
  https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--aarch64.json \
  https://tiny.distro.builders/workload--automotive-workload-off--automotive-environment--automotive-repositories-c9s--x86_64.json \
  --outfile /mnt/rhel9-image-manifest.lock.json
